package com.example.moviii.data.local.database.entity

import androidx.room.*

@Entity(tableName = "user_table")
data class User (
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "userId")
    val id: Int = 0,
    @ColumnInfo(name = "username")
    val username: String,
    @ColumnInfo(name = "email")
    val email: String,
    @ColumnInfo(name = "password")
    val password: String,
    @ColumnInfo(name = "fullname")
    val fullname: String,
    @ColumnInfo(name = "birthdate")
    val birthdate: String,
    @ColumnInfo(name = "address")
    val address: String
)
