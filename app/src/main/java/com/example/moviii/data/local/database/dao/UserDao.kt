package com.example.moviii.data.local.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.moviii.data.local.database.entity.User

@Dao
interface UserDao {
    @Insert
    fun insertUser(user: User): Long

    @Update
    fun updateUser(user: User)

    @Query("SELECT * from user_table WHERE email = :email AND password = :password")
    fun getUserByEmailPassword(email: String, password: String): User?
}