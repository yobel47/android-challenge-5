package com.example.moviii.data

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.moviii.data.local.UserPreferences
import com.example.moviii.data.local.database.MovieDatabase
import com.example.moviii.data.local.database.entity.User
import com.example.moviii.data.remote.response.MovieItem
import com.example.moviii.data.remote.response.MovieResponse
import com.example.moviii.data.remote.retrofit.ApiConfig
import com.example.moviii.data.remote.retrofit.ApiKey
import com.example.moviii.data.remote.retrofit.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DataRepository(
    private val userPreferences: UserPreferences,
    private val movieDatabase: MovieDatabase,
    private val apiService: ApiService
) {
    companion object {
        @Volatile
        private var instance: DataRepository? = null

        @JvmStatic
        fun getInstance(
            pref: UserPreferences,
            movieDatabase: MovieDatabase,
            apiService: ApiService
        ) : DataRepository =
            instance ?: synchronized(this) {
                instance ?: DataRepository(pref, movieDatabase, apiService)
            }.also { instance = it }
    }

    //User
    fun insertUser(user: User): Long {
        var insertResponse: Long = 0
        insertResponse = movieDatabase.userDao().insertUser(user)
        return insertResponse
    }

    fun updateUser(user: User) {
        userPreferences.setUser(user)
        movieDatabase.userDao().updateUser(user)
    }

    fun onLogin(username: String, password:String): User? {
        val loginResponse : User? = movieDatabase.userDao().getUserByEmailPassword(username,password)
        return if(loginResponse==null){
            null
        }else{
            userPreferences.setUser(loginResponse)
            loginResponse
        }
    }

    fun getUser(): User {
        return userPreferences.getUser()
    }

    fun deleteUser() {
        userPreferences.deleteUser()
    }

    @SuppressLint("SuspiciousIndentation")
    fun getMovies(): MutableLiveData<List<MovieItem>?> {
        val listMovies = MutableLiveData<List<MovieItem>?>()
        ApiConfig.getApiService().getMovieNowPlaying(ApiKey.apiKeyTMDB).enqueue(object :
            Callback<MovieResponse> {
            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                val movie = response.body()
                listMovies.postValue(movie?.results!!)
            }
            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                Log.d("Error", "Failure : ${t.message}")
            }
        })
        return listMovies
    }
}