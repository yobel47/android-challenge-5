package com.example.moviii.data.local

import android.content.Context
import com.example.moviii.data.local.database.entity.User

class UserPreferences(context: Context) {

    private val preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun setUser(userData: User) {
        val editor = preferences.edit()
        editor.putInt(ID,userData.id)
        editor.putString(USERNAME,userData.username)
        editor.putString(EMAIL,userData.email)
        editor.putString(PASSWORD,userData.password)
        editor.putString(FULLNAME,userData.fullname)
        editor.putString(BIRTHDATE,userData.birthdate)
        editor.putString(ADDRESS,userData.address)
        editor.apply()
    }

    fun getUser(): User {
        return User(
            preferences.getInt(ID,0),
            preferences.getString(USERNAME,"").toString(),
            preferences.getString(EMAIL,"").toString(),
            preferences.getString(PASSWORD,"").toString(),
            preferences.getString(FULLNAME,"").toString(),
            preferences.getString(BIRTHDATE,"").toString(),
            preferences.getString(ADDRESS,"").toString()
        )
    }

    fun deleteUser(){
        val editor = preferences.edit()
        editor.clear()
        editor.apply()
    }

    companion object {
        private const val PREFS_NAME = "user_pref"
        private const val ID = "id"
        private const val USERNAME = "name"
        private const val EMAIL = "email"
        private const val PASSWORD = "password"
        private const val FULLNAME = "fullname"
        private const val BIRTHDATE = "birthdate"
        private const val ADDRESS = "address"

        @Volatile
        private var instance: UserPreferences? = null

        fun getInstance(context: Context): UserPreferences =
            instance ?: synchronized(this) {
                instance ?: UserPreferences(context)
            }
    }
}