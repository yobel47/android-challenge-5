package com.example.moviii.data.remote.response

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "genre")
data class GenreItem(
    @PrimaryKey
    @field:SerializedName("id")
    val id: Int = 0,

) : Parcelable