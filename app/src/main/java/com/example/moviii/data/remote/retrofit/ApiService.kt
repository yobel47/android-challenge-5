package com.example.moviii.data.remote.retrofit

import com.example.moviii.data.remote.response.MovieItem
import com.example.moviii.data.remote.response.MovieResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {
    @GET("movie/popular")
    fun getMovieNowPlaying(@Query("api_key") apiKey: String?): Call<MovieResponse>
}