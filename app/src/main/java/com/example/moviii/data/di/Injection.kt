package com.example.moviii.data.di

import android.content.Context
import com.example.moviii.data.DataRepository
import com.example.moviii.data.local.UserPreferences
import com.example.moviii.data.local.database.MovieDatabase
import com.example.moviii.data.remote.retrofit.ApiConfig

object Injection {
    fun provideRepository(context: Context): DataRepository {
        val userPreferences = UserPreferences.getInstance(context)
        val database = MovieDatabase.getInstance(context)
        val apiService = ApiConfig.getApiService()
        return DataRepository.getInstance(userPreferences, database, apiService)
    }
}