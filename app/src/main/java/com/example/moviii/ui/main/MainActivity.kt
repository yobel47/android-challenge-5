package com.example.moviii.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.moviii.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}