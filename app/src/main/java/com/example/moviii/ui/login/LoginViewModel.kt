package com.example.moviii.ui.login

import androidx.lifecycle.ViewModel
import com.example.moviii.data.DataRepository
import com.example.moviii.data.local.database.entity.User

class LoginViewModel(
    private val dataRepository: DataRepository,
) : ViewModel() {

    fun getUser() = dataRepository.getUser()

    fun onLogin(username: String, password:String): User? {
        return dataRepository.onLogin(username, password)
    }

}