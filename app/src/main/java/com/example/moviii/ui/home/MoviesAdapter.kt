package com.example.moviii.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviii.R
import com.example.moviii.data.remote.response.MovieItem
import com.example.moviii.databinding.ItemMoviesBinding


class MoviesAdapter(
    private val listMovies: List<MovieItem>?,
    private val fragmentManager: FragmentManager,
) :
    RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    class ViewHolder(var binding: ItemMoviesBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemMoviesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val (overview, _, _, _, title, _, poster, _, release) = listMovies!![position]
        holder.binding.tvTitleMovie.text = title
        holder.binding.tvDescriptionMovie.text = "$overview"
        Glide.with(holder.itemView.context)
            .load("https://image.tmdb.org/t/p/w500/$poster")
            .into(holder.binding.movieImage)
        holder.binding.tvDateMovie.text = release
        holder.binding.cardView.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("title", title)
            bundle.putString("poster", poster)
            bundle.putString("overview", overview)
            it.findNavController().navigate(R.id.detailFragment, bundle)
        }
    }

    override fun getItemCount(): Int  = listMovies!!.size

}