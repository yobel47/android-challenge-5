package com.example.moviii.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.moviii.data.DataRepository
import com.example.moviii.data.remote.response.MovieItem


class HomeViewModel(
    private val dataRepository: DataRepository,
) : ViewModel() {

    fun getUser() = dataRepository.getUser()
    fun deleteUser() = dataRepository.deleteUser()
    fun getAllMovies(): MutableLiveData<List<MovieItem>?> {
        return dataRepository.getMovies()
    }
}