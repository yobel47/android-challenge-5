package com.example.moviii.ui.detail

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.example.moviii.R
import com.example.moviii.databinding.FragmentDetailBinding
import com.example.moviii.databinding.FragmentLoginBinding
import com.example.moviii.ui.ViewModelFactory
import com.example.moviii.ui.login.LoginViewModel

class DetailFragment : Fragment() {

    private lateinit var detailViewModel: DetailViewModel
    private lateinit var binding: FragmentDetailBinding
    private lateinit var sharedPreferences: SharedPreferences
    private val sharedPrefName = "loginSharedPref"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_detail, container, false)

        val factory = ViewModelFactory.getInstance(requireContext())

//        val movies = arguments?.getBundle("movies")

        val title = arguments?.getString("title")
        val poster = arguments?.getString("poster")
        val overview = arguments?.getString("overview")

        binding.tvTittle.text = title.toString()
        binding.tvDescriptionMovie.text = overview.toString()
        Glide.with(requireContext())
            .load("https://image.tmdb.org/t/p/w500/$poster")
            .into(binding.movieImage)

        detailViewModel =
            ViewModelProvider(this, factory)[DetailViewModel::class.java]

        binding.lifecycleOwner = this

        binding.detailViewModel = detailViewModel

        sharedPreferences = requireActivity().getSharedPreferences(sharedPrefName, Context.MODE_PRIVATE)

        return binding.root
    }

}