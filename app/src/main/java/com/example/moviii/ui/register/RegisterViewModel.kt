package com.example.moviii.ui.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.moviii.data.DataRepository
import com.example.moviii.data.local.database.entity.User
import kotlinx.coroutines.launch

class RegisterViewModel(
    private val dataRepository: DataRepository,
) : ViewModel() {
    fun onRegister(user: User) {
        viewModelScope.launch {
            dataRepository.insertUser(user)
        }
    }

}