package com.example.moviii.ui.home

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviii.R
import com.example.moviii.databinding.FragmentHomeBinding
import com.example.moviii.ui.ViewModelFactory
import com.example.moviii.ui.home.HomeViewModel
import com.example.moviii.ui.home.MoviesAdapter
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var binding: FragmentHomeBinding
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_home, container, false)

        val factory = ViewModelFactory.getInstance(requireContext())

        homeViewModel =
            ViewModelProvider(this, factory)[HomeViewModel::class.java]

        binding.lifecycleOwner = this

        binding.homeViewModel = homeViewModel

        val userData = homeViewModel.getUser()

        binding.username = userData.username

        onLogout()
        var adapter : MoviesAdapter
        homeViewModel.getAllMovies().observe(viewLifecycleOwner){
            if (it != null) {
                if(it.isEmpty()){
                    binding.tvRvEmpty.visibility = View.VISIBLE
                }else{
                    binding.tvRvEmpty.visibility = View.INVISIBLE
                }
            }
            adapter = MoviesAdapter(it, childFragmentManager)
            binding.rvMovies.layoutManager = LinearLayoutManager(context)
            binding.rvMovies.setHasFixedSize(true)
            binding.rvMovies.adapter = adapter
        }

        return binding.root
    }


    private fun onLogout(){
        binding.btnProfile.setOnClickListener {
//            homeViewModel.deleteUser()
            findNavController().navigate(R.id.profileFragment, null, NavOptions.Builder()
                .build())
        }
    }
}