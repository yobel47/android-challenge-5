package com.example.moviii.ui.profile

import androidx.lifecycle.ViewModel
import com.example.moviii.data.DataRepository
import com.example.moviii.data.local.database.entity.User

class ProfileViewModel(
    private val dataRepository: DataRepository,
) : ViewModel() {

    fun getUser() = dataRepository.getUser()
    fun deleteUser() = dataRepository.deleteUser()

    fun onUpdate(user: User): Unit {
        return dataRepository.updateUser(user)
    }
}