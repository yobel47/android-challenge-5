package com.example.moviii.ui.profile

import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviii.R
import com.example.moviii.data.local.database.entity.User
import com.example.moviii.databinding.FragmentHomeBinding
import com.example.moviii.databinding.FragmentProfileBinding
import com.example.moviii.ui.ViewModelFactory
import com.example.moviii.ui.home.HomeViewModel
import com.example.moviii.ui.home.MoviesAdapter

class ProfileFragment : Fragment() {

    private lateinit var profileViewModel: ProfileViewModel
    private lateinit var binding: FragmentProfileBinding
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_profile, container, false)

        val factory = ViewModelFactory.getInstance(requireContext())

        profileViewModel =
            ViewModelProvider(this, factory)[ProfileViewModel::class.java]

        binding.lifecycleOwner = this

        binding.profileViewModel = profileViewModel

        val userData = profileViewModel.getUser()

        binding.edRegisterUsername.editText?.setText(userData.username)
        binding.edFullname.editText?.setText(userData.fullname)
        binding.edAddress.editText?.setText(userData.address)
        binding.edBirthdate.editText?.setText(userData.birthdate)

        binding.btnUpdate.setOnClickListener {
            onUpdate(userData)
        }

        onLogout()
        return binding.root
    }


    private fun onUpdate(userData: User){
        val fullname = binding.edFullname.editText?.text
        val birthdate = binding.edBirthdate.editText?.text
        val address = binding.edAddress.editText?.text

        val user = User(userData.id, userData.username, userData.email, userData.password, fullname.toString(), birthdate.toString(), address.toString())
        profileViewModel.onUpdate(user)
    }

    private fun onLogout(){
        binding.btnLogout.setOnClickListener {
            profileViewModel.deleteUser()
            findNavController().navigate(R.id.loginFragment, null, NavOptions.Builder()
                .setPopUpTo(R.id.homeFragment, true)
                .build())
        }
    }

}